package com.example.jorgemanuel.textilesdw;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class StructureActivity extends Activity{

    WebView webView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/eer.html");
        webView.setWebViewClient(new WebViewClient(){
            public  boolean shouldOverrideUrlLoading (WebView view, String url){
                return false;
            }
        });
    }
}
