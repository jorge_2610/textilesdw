package com.example.jorgemanuel.textilesdw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void abrirProductos(View v) {
        Intent abrir = new Intent(getApplicationContext(), ProductsActivity.class);
        startActivity(abrir);
    }

    public void cerrarApp(View v){
        super.onBackPressed();
        finish();
    }

    public  void abrirDiagramaEstructura(View v){
        Intent abrir = new Intent(getApplicationContext(), StructureActivity.class);
        startActivity(abrir);
    }

    public void abrirDiagramaCasosdeUso(View v) {
        Intent abrir = new Intent(getApplicationContext(), UseCaseDiagramActivity.class);
        startActivity(abrir);
    }

    public void abrirSucursales(View v) {
        Intent abrir = new Intent(getApplicationContext(), SucursalesActivity.class);
        startActivity(abrir);
    }

    public void abrirVentas (View v){
        Intent abrir = new Intent(getApplicationContext(), VentasActivity.class);
        startActivity(abrir);
    }
}
