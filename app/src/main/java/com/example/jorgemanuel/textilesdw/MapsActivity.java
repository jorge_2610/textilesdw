package com.example.jorgemanuel.textilesdw;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, Response.Listener<JSONObject>,Response.ErrorListener {

    private GoogleMap mMap;
    ProgressDialog progress;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,latitude,longitude,nombre;
    int cuentaClientes=0,cantidadClientes=0;
    private ViewGroup layout;
    JSONArray json;
    List<LatLng> listaClientes = new ArrayList<LatLng>();
    List<String> listaNombres = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_store);
        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/lista_sucursales.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        agregarMarker(googleMap);
        LatLng cucei = new LatLng(20.657053600, -103.324953900);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cucei));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
        cargarWebService();
    }

    private void agregarMarker(GoogleMap googleMap) {
        mMap = googleMap;
        for (int i = 0; i < listaClientes.size();i++){
            mMap.addMarker(new MarkerOptions()
                    .position(listaClientes.get(i))
                    .title(listaNombres.get(i)));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        json=response.optJSONArray("sucursales");
        try {
            cantidadClientes=json.length();
            while (cuentaClientes<cantidadClientes){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaClientes);
                id=jsonObject.optString("id");
                nombre = jsonObject.optString("nombre");
                latitude=jsonObject.optString("latitude");
                longitude=jsonObject.optString("longitude");
                cuentaClientes++;
                Double lat = Double.parseDouble(latitude);
                Double lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                listaClientes.add(edificio);
                listaNombres.add(nombre);
            }
            progress.hide();
            agregarMarker(mMap);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                    " "+e, Toast.LENGTH_LONG).show();
            progress.hide();
        }

    }
}

