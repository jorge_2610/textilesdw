package com.example.jorgemanuel.textilesdw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {

    ProgressDialog progress;
    private ArrayAdapter<String> adaptador1;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,descripcion,codigo,precio;
    private ViewGroup layout;
    JSONArray json;
    int cuentaClientes=0,cantidadClientes=0;
    LayoutInflater inflater;
    List<String> listaClientes = new ArrayList<String>();
    private ListView lv1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_clientes_activity);

        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        lv1=(ListView)findViewById(R.id.lvproductos);

        cargarWebService();

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent abrir = new Intent(getApplicationContext(),ProductDetailActivity.class);
                abrir.putExtra("descripcion",lv1.getItemAtPosition(position).toString());
                startActivity(abrir);
                ProductsActivity.super.onBackPressed();
                finish();
            }
        });

    }



    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/lista_productos.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        json=response.optJSONArray("productos");
        try {
            cantidadClientes=json.length();
            while (cuentaClientes<cantidadClientes){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaClientes);
                id=jsonObject.optString("id");
                descripcion=jsonObject.optString("descripcion");
                codigo=jsonObject.optString("codigos");
                precio=jsonObject.optString("precio");
                cuentaClientes++;
                listaClientes.add(descripcion);
            }
            progress.hide();
            agregarProductos();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                    " "+e, Toast.LENGTH_LONG).show();
            progress.hide();
        }

    }

    private void agregarProductos() {
        adaptador1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listaClientes);
        lv1.setAdapter(adaptador1);
    }

    public void abrirAddProduct(View v){
        Intent abrir = new Intent(getApplicationContext(),AddProductActivity.class);
        startActivity(abrir);
        ProductsActivity.super.onBackPressed();
        finish();
    }

}
