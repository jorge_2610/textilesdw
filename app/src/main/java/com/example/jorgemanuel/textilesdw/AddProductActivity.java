package com.example.jorgemanuel.textilesdw;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class AddProductActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {
    EditText codigo,precio,descripcion;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product);
        codigo = findViewById(R.id.codigo);
        precio = findViewById(R.id.precio);
        descripcion = findViewById(R.id.descripcion);
        mRequestQueue= Volley.newRequestQueue(this);
    }

    public void addProduct(View v){
        String codigoP,precioP,descripcionP;
        codigoP = codigo.getText().toString();
        precioP = precio.getText().toString();
        descripcionP = descripcion.getText().toString();
        String url="http://textilesdw.x10host.com/querys/agregar_producto.php?codigo="+codigoP +"&precio="+precioP
                +"&descripcion="+descripcionP;
        url = url.replace(" ", "%20");
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
        super.onBackPressed();
        finish();
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {


    }
}
