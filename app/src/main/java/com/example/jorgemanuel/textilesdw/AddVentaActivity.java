package com.example.jorgemanuel.textilesdw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddVentaActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {

    ProgressDialog progress;
    private ArrayAdapter<String> adaptador1;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,descripcion,codigo,precio;
    String idToUpload,priceProduct;
    int total = 0;
    private ViewGroup layout;
    JSONArray json;
    int cuentaClientes=0,cantidadClientes=0;
    LayoutInflater inflater;
    List<String> listaProductos = new ArrayList<String>();
    private Spinner lv1;
    Date currentTime;
    EditText cantidad;
    TextView test;
    boolean dataLoaded = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_venta_activity);
        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        cargarWebService();
        lv1=(Spinner)findViewById(R.id.spinner);
        cantidad = findViewById(R.id.cantidadInput);
        cantidad.setText("0");
        test = findViewById(R.id.totalTV);

        lv1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String text = lv1.getSelectedItem().toString();
                String[] items = text.split(":");
                idToUpload = items[0];
                priceProduct = items[2];
                total = Integer.parseInt(items[2]) * Integer.parseInt(cantidad.getText().toString());
                test.setText(""+total);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        cantidad.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    total = Integer.parseInt(priceProduct) * Integer.parseInt(s.toString());
                    test = findViewById(R.id.totalTV);
                    test.setText("" + total);
                }
                catch (Exception e){
                    test.setText("0");
                }
                if (s.toString()==""){
                    cantidad.setText("0");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
    }



    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/lista_productos.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if(!dataLoaded) {
            Toast.makeText(getApplicationContext(), "Error " + error.toString(), Toast.LENGTH_LONG).show();
            progress.hide();
            dataLoaded = true;
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if(!dataLoaded) {
            json = response.optJSONArray("productos");
            try {
                cantidadClientes = json.length();
                while (cuentaClientes < cantidadClientes) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(cuentaClientes);
                    id = jsonObject.optString("id");
                    descripcion = jsonObject.optString("descripcion");
                    codigo = jsonObject.optString("codigo");
                    precio = jsonObject.optString("precio");
                    cuentaClientes++;
                    listaProductos.add(id + ":" + descripcion + ":" + precio);
                }
                progress.hide();
                agregarProductos();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " " + e, Toast.LENGTH_LONG).show();
                progress.hide();
            }
            dataLoaded = true;
        }

    }

    private void agregarProductos() {
        adaptador1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listaProductos);
        lv1.setAdapter(adaptador1);
    }

    public void addVenta(View view){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(new Date());
        String url="http://textilesdw.x10host.com/querys/agregar_venta.php?idProducto="+idToUpload +"&total="+total
                +"&fecha="+currentTime;
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
        super.onBackPressed();
        finish();
    }
}
