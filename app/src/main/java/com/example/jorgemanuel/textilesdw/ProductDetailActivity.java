package com.example.jorgemanuel.textilesdw;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProductDetailActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {
    ProgressDialog progress;
    TextView tv;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,descripcion,codigo,precio;
    EditText codigoP,precioP,descripcionP;
    String descripcionQuery;
    private ViewGroup layout;
    JSONArray json;
    boolean dataLoaded = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
        String test = getIntent().getStringExtra("descripcion");
        tv = findViewById(R.id.test);
        tv.setText("Mostrando Producto " + test);
        codigoP = findViewById(R.id.codigo);
        precioP = findViewById(R.id.precio);
        descripcionP = findViewById(R.id.descripcion);
        descripcionQuery = test;
        progress = new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        cargarWebService();
    }

    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/producto.php?descripcion="+descripcionQuery;
        url = url.replace(" ", "%20");
        request = new JsonObjectRequest(Request.Method.POST,url,null,this,this);
        mRequestQueue.add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if(!dataLoaded) {
            Toast.makeText(getApplicationContext(), "Error " + error.toString(), Toast.LENGTH_LONG).show();
            progress.hide();
            dataLoaded = true;
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if(!dataLoaded) {
            json = response.optJSONArray("producto");
            try {
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(0);
                id = jsonObject.optString("id");
                descripcion = jsonObject.optString("descripcion");
                codigo = jsonObject.optString("codigo");
                precio = jsonObject.optString("precio");
                codigoP.setText(codigo);
                descripcionP.setText(descripcion);
                precioP.setText(precio);
                progress.hide();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " " + e, Toast.LENGTH_LONG).show();
                progress.hide();
            }
            dataLoaded = true;
        }
    }

    public void modifyProduct(View v){
        String codigoPr,precioPr,descripcionPr;
        codigoPr = codigoP.getText().toString();
        precioPr = precioP.getText().toString();
        descripcionPr = descripcionP.getText().toString();
        String url="http://textilesdw.x10host.com/querys/modificar_producto.php?id="+id +"&codigo="+codigoPr +"&precio="+precioPr
                +"&descripcion="+descripcionPr;
        url = url.replace(" ", "%20");
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
        super.onBackPressed();
        finish();
    }

    public  void deleteProduct(View v){
        String url="http://textilesdw.x10host.com/querys/delete_product.php?id="+id;
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
        super.onBackPressed();
        finish();
    }
}


