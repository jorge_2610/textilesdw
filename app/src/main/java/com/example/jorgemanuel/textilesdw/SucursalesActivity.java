package com.example.jorgemanuel.textilesdw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SucursalesActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {

    ProgressDialog progress;
    private ArrayAdapter<String> adaptador1;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,nombre,ubicacion;
    private ViewGroup layout;
    JSONArray json;
    int cuentaClientes=0,cantidadClientes=0;
    LayoutInflater inflater;
    List<String> listaClientes = new ArrayList<String>();
    private ListView lv1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sucursales_activity);

        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        lv1=(ListView)findViewById(R.id.lvproductos);

        cargarWebService();

        /*lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent abrir = new Intent(getApplicationContext(),ProductDetailActivity.class);
                abrir.putExtra("descripcion",lv1.getItemAtPosition(position).toString());
                startActivity(abrir);
                SucursalesActivity.super.onBackPressed();
                finish();
            }
        });*/

    }



    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/lista_sucursales.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        json=response.optJSONArray("sucursales");
        try {
            cantidadClientes=json.length();
            while (cuentaClientes<cantidadClientes){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaClientes);
                id=jsonObject.optString("id");
                nombre=jsonObject.optString("nombre");
                ubicacion=jsonObject.optString("ubicacion");
                cuentaClientes++;
                listaClientes.add(id + ". "+ nombre + ", " + ubicacion);
            }
            progress.hide();
            agregarSucursales();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                    " "+e, Toast.LENGTH_LONG).show();
            progress.hide();
        }

    }

    private void agregarSucursales() {
        adaptador1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listaClientes);
        lv1.setAdapter(adaptador1);
    }

    public void salir(View v){
        SucursalesActivity.super.onBackPressed();
        finish();
    }

    public void verMapa(View v){
        Intent abrir = new Intent(getApplicationContext(),MapsActivity.class);
        startActivity(abrir);
    }

}