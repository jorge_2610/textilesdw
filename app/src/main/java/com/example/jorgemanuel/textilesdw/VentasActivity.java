package com.example.jorgemanuel.textilesdw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VentasActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {

    ProgressDialog progress;
    private ArrayAdapter<String> adaptador1;
    JsonObjectRequest request;
    RequestQueue mRequestQueue;
    private String id,fecha,idProducto,total;
    private ViewGroup layout;
    JSONArray json;
    int cuentaClientes=0,cantidadClientes=0;
    LayoutInflater inflater;
    List<String> listaClientes = new ArrayList<String>();
    private ListView lv1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ventas_activity);

        progress=new ProgressDialog(this);
        mRequestQueue= Volley.newRequestQueue(this);
        lv1=(ListView)findViewById(R.id.lvproductos);

        cargarWebService();


    }



    private void cargarWebService() {
        progress.setMessage("Cargando...");
        progress.show();
        String url="http://textilesdw.x10host.com/querys/ventas.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error "+error.toString(), Toast.LENGTH_LONG).show();
        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        json=response.optJSONArray("ventas");
        try {
            cantidadClientes=json.length();
            while (cuentaClientes<cantidadClientes){
                JSONObject jsonObject=null;
                jsonObject=json.getJSONObject(cuentaClientes);
                id=jsonObject.optString("id");
                fecha=jsonObject.optString("fecha");
                total=jsonObject.optString("total");
                idProducto=jsonObject.optString("idProducto");
                cuentaClientes++;
                listaClientes.add(id +".Fecha:"+fecha +", Producto: "+idProducto + ", Total: "+total);
            }
            progress.hide();
            agregarProductos();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                    " "+e, Toast.LENGTH_LONG).show();
            progress.hide();
        }

    }

    private void agregarProductos() {
        adaptador1=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listaClientes);
        lv1.setAdapter(adaptador1);
    }

    public void abrirAddVenta(View v){
        Intent abrir = new Intent(getApplicationContext(),AddVentaActivity.class);
        startActivity(abrir);
        VentasActivity.super.onBackPressed();
        finish();
    }

}
