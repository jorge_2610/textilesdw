package com.example.jorgemanuel.textilesdw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class UseCaseDiagramActivity extends AppCompatActivity{

    WebView webView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ucd_activity);
        webView = (WebView) findViewById(R.id.vistaWeb);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/ucd.html");
        webView.setWebViewClient(new WebViewClient(){
            public  boolean shouldOverrideUrlLoading (WebView view, String url){
                return false;
            }
        });
    }

}
